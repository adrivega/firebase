package com.example.segundo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.segundo.ui.theme.SegundoTheme

class Mostrar : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SegundoTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    Huerto()
                }
            }
        }
    }
}

@Composable
fun Huerto(){
    var id by remember {
        mutableStateOf(R.drawable.huerto_vacio)
    }
    var contador by remember {
        mutableStateOf(1)
    }
    Column() {
        Image(
            painter = painterResource(id = id),
            contentDescription = "Huerto"
        )
        Button(
            onClick = {
                if (contador == 1){
                    id = R.drawable.huerto_vacio
                    contador++
                }else if(contador == 2){
                    id = R.drawable.huerto_plantas
                    contador++
                }else if (contador == 3){
                    id = R.drawable.huerto_frutas
                    contador = 1
                }
            })
        {
            Text(text = "Boton")
        }
    }
}
@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    SegundoTheme {
        Huerto()
    }
}

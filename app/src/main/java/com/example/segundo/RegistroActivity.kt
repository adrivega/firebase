package com.example.segundo

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.segundo.ui.theme.SegundoTheme
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class RegistroActivity : ComponentActivity() {

    private lateinit var firebaseAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        firebaseAuth = FirebaseAuth.getInstance()

        setContent {
            SegundoTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    RegistroView()
                }
            }
        }
    }

    fun verifyEmail(user: FirebaseUser) {
        user.sendEmailVerification().addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                Toast.makeText(this, "Email verificado", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Error al verificar email", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun registro(email: String, pass: String) {
        if (email.isNotEmpty() && pass.isNotEmpty()) {
            //damos de alta email y contraseña
            firebaseAuth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(this) {
                //obtener el id de usuario, sabemos que no es nulo ya que lo acabamos de crear
                val user: FirebaseUser = firebaseAuth.currentUser!!
                verifyEmail(user)
                //tdo ha salido bien, por lo tanto vamos a mostrar
                startActivity(Intent(this, Mostrar::class.java))
            }.addOnFailureListener {
                Toast.makeText(this, "Error de autenticación", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Inserta los datos bien", Toast.LENGTH_SHORT).show()
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun RegistroView() {
        var email by remember {
            mutableStateOf("")
        }
        var password by remember {
            mutableStateOf("")
        }
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally

        ) {
            TextField(
                value = email,
                onValueChange = { email = it },
                label = { Text(text = "Email") },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Email
                ),
                modifier = Modifier.padding(10.dp)
            )

            TextField(
                value = password,
                onValueChange = { password = it },
                label = { Text(text = "Password") },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Password
                ),
                modifier = Modifier.padding(10.dp)
            )
            Button(onClick = { registro(email, password) }) {
                Text(text = "Registro")
            }
        }
    }

    @Preview(showBackground = true)
    @Composable
    fun RegistroPreview() {
        SegundoTheme {
            RegistroView()
        }
    }
}


